defmodule LibrarySystemWeb.PageController do
  use LibrarySystemWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
