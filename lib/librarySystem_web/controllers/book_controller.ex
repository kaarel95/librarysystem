defmodule LibrarySystemWeb.API.BookController do

  use LibrarySystemWeb, :controller
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]
  alias LibrarySystem.{Repo,Book}

  def extend_book(conn, %{"book" => book}) do
     book = Repo.get_by(Book, title: book)
     if book.extended|>String.to_integer ==4 do 
        put_status(conn, 201) |> json(%{message: "book already extended 4 times"})
    else
        case book.code do 
            "A" -> 
                uus = String.split(book.due,"-")
                date = Date.from_iso8601!(Enum.at(uus,2)<>"-"<>Enum.at(uus,1)<>"-"<>Enum.at(uus,0))
                new_date = Date.to_string(Date.add(date,7)) 
                string_date = String.split(new_date, "-")
                Repo.update(change book, %{due: (Enum.at(string_date,2)<>"-"<>Enum.at(string_date,1)<>"-"<>Enum.at(string_date,0)) })
            "B" -> 
                uus = String.split(book.due,"-")
                date = Date.from_iso8601!(Enum.at(uus,2)<>"-"<>Enum.at(uus,1)<>"-"<>Enum.at(uus,0))
                new_date = Date.to_string(Date.add(date,14)) 
                string_date = String.split(new_date, "-")
                Repo.update(change book, %{due: (Enum.at(string_date,2)<>"-"<>Enum.at(string_date,1)<>"-"<>Enum.at(string_date,0)) })
            "C" -> 
                uus = String.split(book.due,"-")
                date = Date.from_iso8601!(Enum.at(uus,2)<>"-"<>Enum.at(uus,1)<>"-"<>Enum.at(uus,0))
                new_date = Date.to_string(Date.add(date,21)) 
                string_date = String.split(new_date, "-")
                Repo.update(change book, %{due: (Enum.at(string_date,2)<>"-"<>Enum.at(string_date,1)<>"-"<>Enum.at(string_date,0)) })
        end        
        Repo.update(change book, %{extended: to_string( (book.extended |> String.to_integer) +1)})
        
        put_status(conn, 201) |> json(%{message: "extended"})
     end    
  end

  def get_books(conn, _params) do
    query = from d in Book, select: d
    books = Repo.all(query)
    put_status(conn, 201) |> json(%{books: books})
  end

    
end