defmodule LibrarySystemWeb.Router do
  use LibrarySystemWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", LibrarySystemWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/api", LibrarySystemWeb do
      pipe_through :api
      post "/extend", API.BookController, :extend_book
      get "/books", API.BookController, :get_books
  end


  # Other scopes may use custom stacks.
  # scope "/api", LibrarySystemWeb do
  #   pipe_through :api
  # end
end
