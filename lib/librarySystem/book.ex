defmodule LibrarySystem.Book do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:code, :due, :extended, :start, :title, :open]}
  schema "books" do
    field :code, :string
    field :due, :string
    field :extended, :string
    field :start, :string
    field :title, :string
    field :open, :string
    timestamps()
  end

  @doc false
  def changeset(book, attrs) do
    book
    |> cast(attrs, [:title, :code, :start, :due, :extended, :open])
    |> validate_required([:title, :code, :start, :due, :extended, :open])
  end
end
