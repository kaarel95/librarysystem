# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     LibrarySystem.Repo.insert!(%LibrarySystem.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.



alias LibrarySystem.Repo
alias LibrarySystem.Book

Repo.insert!(%Book{title: "Programming Phoenix 13", code: "A",start: "09-01-2018", due: "16-01-2018", extended: "0", open: "true"})
Repo.insert!(%Book{title: "Secrets of the JavaScript Ninja", code: "A",start: "19-12-2017", due: "16-01-2018", extended: "4", open: "false"})