defmodule LibrarySystem.Repo.Migrations.CreateBooks do
  use Ecto.Migration

  def change do
    create table(:books) do
      add :title, :string
      add :code, :string
      add :start, :string
      add :due, :string
      add :extended, :string
      add :open, :string

      timestamps()
    end

  end
end
