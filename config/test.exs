use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :librarySystem, LibrarySystemWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :librarySystem, LibrarySystem.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "librarysystem_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox


config :hound, driver: "chrome_driver"
config :extend, sql_sandbox: true
