defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  alias LibrarySystem.{Repo,Book}

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)    
    %{}
  end
  scenario_starting_state fn state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(LibrarySystem.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(LibrarySystem.Repo, {:shared, self()})
    %{}
  end

  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(LibrarySystem.Repo)
    # Hound.end_session
  end 

  given_ ~r/^the following books are in the list$/, 
  fn state, %{table_data: table} ->
    #table 
    #|> Enum.map(fn book -> Book.changeset(%Book{}, book) end)
   # |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^i open web page$/, fn state ->
    navigate_to "/"
    {:ok, state}
  end

  and_ ~r/^i select the book "(?<argument_one>[^"]+)"$/,
    fn state, %{argument_one: _argument_one} ->
      click({:id, "extend_"<>state[:argument_one]})
      {:ok, state}
  end

  when_ ~r/^i extend$/, fn state ->
    {:ok, state}
  end 

  then_ ~r/^the Time extended should be 1$/, fn state ->
    assert visible_in_page? ~r/extended/    
    {:ok, state}
  end

  then_ ~r/^i should see error message$/, fn state ->
  assert visible_in_page? ~r/book already extended 4 times/
    {:ok, state}
  end


end
