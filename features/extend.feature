Feature: Borrowing a book


   As a borrower
   Such that i want to extend a book
   I want to see the books i have borrowed

       Scenario: extend a book
            Given the following books are in the list
            |title|code|Start date|Due date|Times extended|
            |Programming phoenix 13| A|09-01-2018|23-01-2018|1|
            |Secrets of the javascript ninja|A|19-12-2017|16-01-2018|4|
        And i open web page
        And i select the book "Programming phoenix 13"
        When i extend
        Then the Time extended should be 1

        Scenario: cant extend a book
            Given the following books are in the list
                |title|code|Start date|Due date|Times extended|
                |Programming phoenix 13| A|09-01-2018|23-01-2018|1|
                |Secrets of the javascript ninja|A|19-12-2017|16-01-2018|4|
        And i open web page
        And i select the book "Secrets of the javascript ninja"     
        When i extend
        Then i should see error message
