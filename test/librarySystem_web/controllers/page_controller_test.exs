defmodule LibrarySystemWeb.PageControllerTest do
  use LibrarySystemWeb.ConnCase
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]
  alias LibrarySystem.{Repo,Book}

  test "POST - extend a book, and date", %{conn: conn} do
    book = Repo.insert!(%Book{title: "Programming Phoenix 13", code: "A",start: "09-01-2018", due: "16-01-2018", extended: "0", open: "true"})
    request = %{"book" => book.title}
    response =
        conn
        |> post(book_path(conn, :extend_book, request))
        |> json_response(201)

    expected = %{"message" => "extended"}
    assert response = expected

    book_new = Repo.get_by(Book, title: "Programming Phoenix 13")
    assert book_new.due == "23-01-2018"

  end

  test "POST - cannot extend a book, date does not change", %{conn: conn} do
    book = Repo.insert!(%Book{title: "Secrets of the JavaScript Ninja", code: "A",start: "09-01-2018", due: "16-01-2018", extended: "4", open: "true"})
    request = %{"book" => book.title}
    response = conn |> post(book_path(conn, :extend_book, request))|>json_response(201)
    expected = %{"message" => "book already extended 4 times"}
    assert response = expected

    book_new = Repo.get_by(Book, title: "Secrets of the JavaScript Ninja")
    assert book_new.due == "16-01-2018"
  end
end
